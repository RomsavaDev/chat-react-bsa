import React from "react";
import "./Header.css";

export default class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="Header">
        <div>My chat</div>
        <div>{`Participantes : ${this.props.participantes + 1}`}</div>
        <div>{`Messages : ${this.props.messages}`}</div>
        <div>{`Last message at : ${this.props.lastMessages}`}</div>
      </div>
    );
  }
}
