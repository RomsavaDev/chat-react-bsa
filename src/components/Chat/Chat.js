import React from "react";
import { getMessageFromAPI } from "../../helpers/api-helper";
import Header from "../Header/Header";
import MessageInput from "../Message-input/MessageInput";
import MessageList from "../Message-list/MessageList";
import "./Chat.css";

export default class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: null,
    };
  }

  componentDidMount() {
    getMessageFromAPI().then((data) =>
      this.setState({
        messages: data,
      })
    );
  }

  getParticipantes = () => {
    const participantes = new Set();
    console.log(this.state.messages);
    Array.prototype.forEach.call(this.state.messages, (element) => {
      participantes.add(element.user);
    });
    return participantes.size;
  };

  addNewMessage = (obj) => {
    this.setState((prevState) => {
      return { messages: prevState.messages.push(obj) };
    });
  };

  getLastMessage = () => {
    const arrayOfData = this.state.messages.map((el) =>
      Date.parse(el.createdAt)
    );
    console.log(arrayOfData.sort()[arrayOfData.length - 1]);
    const dateOflastMessage = new Date(
      arrayOfData.sort()[arrayOfData.length - 1]
    );
    const lastMessage = `${dateOflastMessage.getHours()} : ${dateOflastMessage.getMinutes()}`;
    return lastMessage;
  };

  render() {
    if (this.state.messages === null) {
      return <div>LOADING...</div>;
    } else {
      return (
        <div className="Chat">
          <Header
            participantes={this.getParticipantes()}
            messages={this.state.messages.length}
            lastMessages={this.getLastMessage()}
          ></Header>
          <MessageList messages={this.state.messages}></MessageList>
          <MessageInput addNewMessage={this.addNewMessage}></MessageInput>
        </div>
      );
    }
  }
}
